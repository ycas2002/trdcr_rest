var express = require('express');
var router = express.Router();

router.get("/", function(req, res, next) {
    if(req.err){
        res.json(req.err);
    } else {
        let obj = require('../tables/car_purchases');
        res.send(obj);
    }
    
});
//
//router.post("/", function(req, res, next){

//});
//router.put("/:id", function(req, res, next){

//});
//router.delete('/:id', function(req, res, next){

//});
module.exports = router;